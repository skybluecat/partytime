var testme;
var mylist;

var stateInformation = {
 //"playerAlive" : true
 //now doesn't make sense
};

var gameVariables = {
	"gameOver" : false,
	"endingText" : "NA",
	"turnNumber" : 0,
	"numIntents" : 2,
	"numActionsPerIntent" : 5
};

var setupCharacterDescriptions = function() {
	var mydiv;
	for (var i=0;i<cast.length;i++)
	{
		mydiv=document.getElementById(cast[i]);
		mydiv.innerHTML= cast[i]+ getCharacterDescription(cast[i]);
	}
};


var setUpInitialState = function(){
	//update our local copies of these variables, and display them.
	updateLocalStateInformation();
};

//Fills in all of the actionList divs with buttons corresponding to the actions the player can take
//by calling individual instances of populateActionList
var populateActionLists = function(storedVolitions, cast){
	for(var i=0;i<cast.length;i++)
	{populateActionList("player", cast[i], storedVolitions, cast);}
};

//Fills the actionList div with buttons corresponding to the actions the player can take.
var populateActionList = function(initiator, responder, storedVolitions, cast){
	var char1 = initiator;
	var char2 = responder;
	
	//Num intents to look at: 5
	//Num actions per intent: 2 (for now!)
	//console.log("storedVolitions before getting possible actions... " , storedVolitions.dump());
	var possibleActions = cif.getActions(char1, char2, storedVolitions, cast, gameVariables.numIntents, gameVariables.numActionsPerIntent);


	
	var mydiv=document.getElementById(char2);

	var tempp;mydiv.innerHTML+="<br/>Actions to "+char2+": ";
	//Let's make a button for each action the hero wants to take!
	for(var i = 0; i < possibleActions.length; i += 1){
		//Make a new button
		var action = possibleActions[i];

		//If the character doesn't have a strong volition to do this action,
		//don't include it in the action list.
		if(action.weight < 0){
			continue;
		}
		var buttonnode= document.createElement('input');
		buttonnode.setAttribute('type','button');
		buttonnode.setAttribute('name',action);
		buttonnode.setAttribute('value',action.displayName);
		buttonnode.actionToPerform = action;
		buttonnode.cast = cast;
		buttonnode.onclick = actionButtonClicked;
		//buttonnode.attachEvent('onclick', actionButtonClicked2);


		mydiv.appendChild(buttonnode);
	}

	//Write a little message if there were no possible actions.
	if(possibleActions.length==0){
		
	mydiv.innerHTML+="No actions possible";
	}

};

var actionButtonClicked = function(){
	//Clean away all of the other actions -- they made their choice!
	clearActionList();


	//CHANGE THE SOCIAL STATE -- social physics baby!!!
	var effects = this.actionToPerform.effects; // should be an array of effects.
	for(var i = 0; i < effects.length; i += 1){
		cif.set(effects[i]);
	}
	//RUN SOME TRIGGER RULES based on the new state!
	cif.runTriggerRules(this.cast);
	
	//Print out if the action was 'accepted' or rejected!
	var statusArea = document.getElementById("actionsMessage");
	var acceptMessage = this.actionToPerform.displayName + " successful!";
	if(this.actionToPerform.isAccept !== undefined && this.actionToPerform.isAccept === false){
		acceptMessage = this.actionToPerform.displayName + " failed!";
	}
	statusArea.innerHTML = acceptMessage;
	
	/////////////////////
	//other characters get to act now!
	/////////////////////
	var storedVolitions = cif.calculateVolition(cast);
	var NPCActions = new Array();
	var tempAction;
	var i;var j;
	for(i = 0; i < cast.length; i += 1){
		if(cast[i] == "player"){continue;}
		for(j = 0; j < cast.length; j += 1){
			NPCActions=NPCActions.concat(cif.getActions(cast[i], cast[j], storedVolitions, cast, 2, 1));
		}
		console.log("This is what "+cast[i]+" wants to do:");
		console.log(NPCActions);
		NPCActions=NPCActions.filter(function(a){return(a.weight >= 0)});
		if(NPCActions.length==0){
			console.log("nonono! this guy has no actions! it's the rule writer's fault!");
			continue;
		}
		tempAction = NPCActions[Math.floor(Math.random()*NPCActions.length)];
		effects = tempAction.effects; // should be an array of effects.
		for(j = 0; j < effects.length; j += 1){
			cif.set(effects[j]);
		}
		statusArea.innerHTML += "  "
		acceptMessage = tempAction.displayName + " (by " + tempAction.goodBindings[0].initiator + " to "+ tempAction.goodBindings[0].responder + ") successful!";
		if(tempAction.isAccept !== undefined && tempAction.isAccept === false){
			acceptMessage = tempAction.displayName + " (by " + tempAction.goodBindings[0].initiator + " to "+ tempAction.goodBindings[0].responder + ") failed!";
		}
		//RUN SOME TRIGGER RULES based on the new state!
		cif.runTriggerRules(cast);
		statusArea.innerHTML += acceptMessage;
		NPCActions=new Array();
		
	}
	
	

	

	

	
	

	//Re-draw the people (maybe even by having them MOVE to their new positions...)
	//Also re-draw any debug/state informaton we want.
	updateLocalStateInformation();

	//set up next turn.
	var event = document.createEvent('Event');
	event.initEvent('nextTurn', true, true);
	document.dispatchEvent(event);
};

var clearActionList = function(){
	for(var i=0;i<cast.length;i++)
	{
		document.getElementById(cast[i]).innerHTML="";
	}
};

//Checks to see if the game is over!
var checkForEndConditions = function(){
	if(stateInformation.playerAlive == false){
		//uh oh, we lose!
		gameVariables.gameOver = true;
		gameVariables.endingText = "Game Over! You are dead!";
	}
	if(gameVariables.turnCount>=20)
	{
		//player survived!
		gameVariables.gameOver = true;
		gameVariables.endingText = "Time's up!";
	}

};

//There are certain things that we might need to 'refresh' again (the visibility of the action list,
//the state of dialogue bubbles, etc.)
var cleanUpUIForNewTurn = function(){
};

var updateLocalStateInformation = function(){
//now the player can't be killed because he's a ghost
/*	var playerAlivePred = {
		"class" : "trait",
		"type" : "alive",
		"first" : "player",
		"value" : true,
	};

	var results;
	results=cif.get(playerAlivePred);
	if(results.length>0){stateInformation.playerAlive = true;}
	else{stateInformation.playerAlive=false;}
	*/

};

var getCharacterDescription = function(character){
	var desc=""; var results;
	var query;
	var cats;var types;//categories and types in the schema
	var tempcat;var temptype;
	for(cats=0;cats<rawSchema.schema.length;cats++)
	{
		tempcat=rawSchema.schema[cats];
		if (tempcat.directionType=="undirected")
		{
			if(tempcat.isBoolean==false)
			{
				//undirected number value
				for(types=0;types<tempcat.types.length;types++)
				{
					temptype=tempcat.types[types];
					query = {
					"class" : tempcat.class,
					"type" : temptype,
					"first" : character
					};
					results=cif.get(query);
					if((results.length>0) && typeof(results[0].value)!= "undefined"){desc+=", "+temptype+": "+results[0].value.toString();}
				}
			}
			else
			{
				//undirected boolean value
				for(types=0;types<tempcat.types.length;types++)
				{
					temptype=tempcat.types[types];
					query = {
					"class" : tempcat.class,
					"type" : temptype,
					"first" : character,
					"value" :true
					};
					results=cif.get(query);
					if((results.length>0) && typeof(results[0].value)!= "undefined"){desc+=", "+temptype;}
					else{desc+=", not "+temptype;}
				}
			}
		}
	}
	return desc;
}
